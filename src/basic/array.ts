let fibonacci :number[]=[1,2,3,4,5];//类型+[]表示法
//数组的项中不允许出现其它类型
fibonacci.push(6);//push方法值允许传入number类型的参数，传字符串类型会报错；
//参数也会根据数组在定义时约定的类型进行限制；
console.log(fibonacci);


// 二：数组泛型


//三：用接口表示数组：
// interface numberArray{
//     [index:number]:number;
// }
// let fibonacciNum:numberArray=[1,2,3,4]

// 4.array在数组中的应用
export let list:any[]=['jump',8,{website:'http://www.baidu.com'}];
// 5.类数组
//类数组不是数组类型
// function suma(){
//     let args:number[]=arguments;
// }
