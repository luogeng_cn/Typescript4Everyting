// import Query from "./server/query";
// const q= new Query('query')
// console.log(q.b,Query.say())
function hello(person: string) {
    return 'Hello' + person;
}
let user = "Tom";
let isDone: boolean = true;
//注：使用构造函数Boolean创造的不是布尔值,因为它返回的是一个对象类型为object,是一个boolean对象
// let createByNewBoolean: boolean = new Boolean(1);
let createByBoolean: boolean = Boolean(1);
let str :string= String(1)
let age: number = 25;
let lname: string = 'luoooo';
let lisence = `Hello,my name is ${lname} ,i'm${age}years old `;
// ?????此处的函数内void取了何意？？？
function alertName(): void {
    alert('My name is Tom')
}
let unusable: void = undefined;
let u: undefined = undefined;
let n: null = null;
// let num: number = u;//注：阮一峰说undefined可以被赋值给number 但我这里报错？？？
let voidOne: void;
// let num: number = voidOne;