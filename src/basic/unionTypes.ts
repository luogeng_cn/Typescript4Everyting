let myFavoriteNumber: string | number;
myFavoriteNumber = "seven";
myFavoriteNumber = 7;
let stringOrNum: string | number;
// aa = true;
//联合类型使用|分隔每个类型；
//这里的let myFavoriteNumber: string | number的含义是：允许其类型是string或者number，但是不能是其它类型；
function getLength(something: string | number) {
    // return something.length;
}
//当ts不确定一个联合类型的变量到底是那个类型的时候，我们只能返回此联合类型的所有类型里共有的属性或方法；
function getString(something: string | number) {
    return something.toString();
}
let bb: string | number;
bb = "seven";
bb=10;
// console.log(bb.length);

