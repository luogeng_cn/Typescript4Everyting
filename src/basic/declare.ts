///我们需要declare关键字来定义它的类型，帮助ts判断传入的参数类型对不对
// 1.声明语句
declare var underscore:(selector:string)=>any;
// jQuery('#foo');
declare var _:(selector:string)=>string;
// $('#add')

// 2.声明文件
// 我们会将类型声明放到一个单独的文件中，这就是声明文件

// jQuery.d.ts

//注：我们约定声明文件以.d.ts为后缀，
// 然后在使用到的文件的开头，用【三斜线指令】表示引用了声明文件
// declare var jQuery:(string:string)=>any;
///<reference path="./jQuery.d.ts" />
// jQuery('#foo');
// $('div').on()

// _.isString(1)
