//手动指定一个值的类型
//将一个联合类型的变量指定为一个更加具体的类型
function getLength1(something: string | number): number {
    if ((<string>something).length) {
        return (<string>something).length;
    } else {
        return something.toString().length;
    }
}
//如上：类型断言的用法为在需要断言的变量前加上<Type>即可；
//类型断言不是类型转换，断言成一个联合类型中不存在的类型是不允许的
function toBoolean(something: string | number): string {
    return <string>something;
}