//接口是对行为的抽象，而具体应如何行动应需要类去实现
interface Person {
    name: string;
    age?: string;
    [propName: string]: string | undefined;
}

let Tom: Person = {
    name: "Tom",
    age: '25',
    add: '34'
}

//赋值的时候，变量的形状必须和接口的形状保持一致;
//定义了接口Person,接着定义了一个变量Tom,其类型是Person。
//这样就约束了变量的形状必须和接口一致。
//接口一般首字母大写;
//属性的后面加?：表示是可选属性=>含义是该属性可以不存在;

//使用了[propName:string]定义了任意属性取string类型的值;
// 注：一旦定义了任意属性。那么确定属性和可选属性都必须是它的子属性;
interface Student {
    readonly name: string;
    age: number;
    [propName: string]: any;
}
let baby: Student = {
    name: 'anlll',
    age:18
}
// baby.name ='eni';//报错啦！！！Cannot assign to 'name' because it is a read-only property.
