let b:Boolean = new Boolean(1);
let e:Error = new Error('Error occurred');
let d:Date = new Date();
let r:RegExp = /[a-z]/;


//DOM和BOM的内置对象：document   HTMLElement   Event  NodeList
let body:HTMLElement = document.body;
let allDiv:NodeList = document.querySelectorAll('div');
document.addEventListener('click',function(e:MouseEvent){});


//浏览器环境需要用到的类型，预置在ts中
Math.pow(10,2);
document.addEventListener('click',function(e){
    console.log(e.currentTarget);
})
