let myFavoriteString :string= 'seven';
//如果为任意值类型，可以被赋值为任意类型
let myFavoriteAny :any= 3;
myFavoriteAny = "bbi";
let anyThing : any = "hello";
console.log(anyThing.myName);
console.log(anyThing.myName.firstName);
//声明一个值为任意值之后。对他的任何操作。返回的类型都是任意值；
let someThing;
someThing = 'cfnwu';
someThing =5;
someThing={
    name:"bih",
    setName:function(a:string){
        return  a ;
    }
}
someThing.setName('Tom')