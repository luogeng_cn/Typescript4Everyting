export default class Query {
    b: string = 'b'
    static c: string | undefined = 'c'
    /**
     * 简单查询类
     * @param parameters 给b的值
     */
    constructor(parameters?: string) {
        parameters && (this.b = parameters);
    }
    /**
     * 打印静态的变量
     */
    static say() {
        console.log(`i'm query is ${this.c}`)
    }
}